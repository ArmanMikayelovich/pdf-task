package am.mikayelovich.task.service.impl;

import am.mikayelovich.task.service.PdfService;
import am.mikayelovich.task.service.StampService;
import am.mikayelovich.task.util.exceptions.ServerSideException;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Image;
import com.j256.simplemagic.ContentInfo;
import com.j256.simplemagic.ContentInfoUtil;
import com.j256.simplemagic.ContentType;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.List;


@Service
public class PdfServiceImpl implements PdfService {
    private static final String OUTPUT_FILE_NAME = "output.pdf";

    private final StampService stampService;

    public PdfServiceImpl(StampService stampService) {
        this.stampService = stampService;
    }

    /**
     * Copies all pages from sources to destination file
     *
     * @param sources         List of source file paths
     * @param writingDocument destination pdf document
     */
    void copyPagesToDestinationPdf(List<String> sources, PdfDocument writingDocument) {
        for (String origin : sources) {
            try (final PdfReader pdfReader = new PdfReader(origin);
                 final PdfDocument readingDocument = new PdfDocument(pdfReader)) {

                readingDocument.copyPagesTo(1, readingDocument.getNumberOfPages(), writingDocument);
            } catch (IOException exception) {
                throw new ServerSideException(exception.getMessage());
            }

        }
    }


    /**
     * Adds timestamp in every page of PDF document
     *
     * @param writingDocument PDF document
     */
    private void addTimestampToPdfPages(PdfDocument writingDocument) {
        final Document document = new Document(writingDocument);
        final int pagesCount = writingDocument.getNumberOfPages();
        for (int offset = 1; offset <= pagesCount; offset++) {
            final Image image = stampService.getImageQrCode(writingDocument);
            image.setFixedPosition(offset, 5, 5);
            document.add(image);
        }
    }

    /**
     * Uses simplemagic library for checking files extensions
     *
     * @param sources List of source file paths
     * @see <a href="http://256stuff.com/sources/simplemagic/">Mime Type Detection Java Package</a>
     */
    private void checkSourcesFileExtensions(List<String> sources) {

        sources.forEach(source -> {
            ContentInfoUtil util = new ContentInfoUtil();
            File possiblePdfFile = new File(source);
            ContentInfo info;
            try {
                info = util.findMatch(possiblePdfFile);
            } catch (IOException exception) {
                throw new ServerSideException(exception.getMessage());
            }
            if (!ContentType.PDF.equals(info.getContentType())) {
                throw new IllegalArgumentException(source + " is not PDF file.");
            }
        });
    }

    /**
     * Uses java IO for checking is files exists or not.
     * @param sources List of source file paths
     */
    private void checkSourcesExisting(List<String> sources) {
        sources.forEach(source -> {
            final File file = new File(source);
            if (!file.exists()) {
                throw new IllegalArgumentException(source + " file not exists.");
            }
        });
    }

    /**
     * Checks file paths list is empty or not
     * @param sources List of source file paths
     */
    private void checkSourceListNotEmpty(List<String> sources) {
        if (sources.isEmpty()) {
            throw new IllegalArgumentException("File list should not be empty");
        }
    }

    /**
     * Processing PDF file includes:
     * <p><b>Source list checking</b>,</p>
     * <p><b>checking files existence</b>,</p>
     * <p><b>checking files extension</b>,</p>
     * <p><b>creating output.pdf file in project root folder</b>,</p>
     * <p><b>copying all pages from sources to output.pdf</b>,</p>
     * <p><b>add timestamp in every page of output.pdf</b>.</p>
     *
     * @param sources List of source files paths
     * @return Absolute path of output.pdf in file system
     */
    @Override
    public String checkAndProcessFiles(List<String> sources) {

        checkSourceListNotEmpty(sources);
        checkSourcesExisting(sources);
        checkSourcesFileExtensions(sources);

        try (final PdfWriter pdfWriter = new PdfWriter(OUTPUT_FILE_NAME);
             final PdfDocument writingDocument = new PdfDocument(pdfWriter)) {


            copyPagesToDestinationPdf(sources, writingDocument);
            addTimestampToPdfPages(writingDocument);

            return new File(OUTPUT_FILE_NAME).getAbsolutePath();

        } catch (IOException exception) {
            throw new ServerSideException(exception.getMessage());
        }

    }

}
