package am.mikayelovich.task.service.impl;

import am.mikayelovich.task.service.StampService;
import com.itextpdf.barcodes.BarcodeQRCode;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.layout.element.Image;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class StampServiceImpl implements StampService {
    /**
     * Creates ITextPDF 7 Image object with timestamp QR code
     * Uses ITextPDF 7 qr code generator
     *
     * @param doc document where need include timestamp
     * @return 50x50 qr code image
     * @see Image
     */
    public Image getImageQrCode(PdfDocument doc) {
        final String timestamp = LocalDateTime.now().toString().replace("T", " ");
        final BarcodeQRCode barcodeQRCode =
                new BarcodeQRCode(timestamp);
        final float width = barcodeQRCode.getBarcodeSize().getWidth();
        int qrCodeSize = 50;
        float scaleSize = qrCodeSize / width;
        final Image image = new Image(barcodeQRCode.createFormXObject(com.itextpdf.kernel.color.Color.BLACK, doc));
        image.scale(scaleSize, scaleSize);
        return image;
    }
}
