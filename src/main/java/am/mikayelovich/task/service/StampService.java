package am.mikayelovich.task.service;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.layout.element.Image;

public interface StampService {

    Image getImageQrCode(PdfDocument doc);
}
