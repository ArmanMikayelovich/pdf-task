package am.mikayelovich.task.service;

import java.io.IOException;
import java.util.List;

public interface PdfService {

    String checkAndProcessFiles(List<String> sources) throws IOException;
}
