package am.mikayelovich.task.conroller;

import am.mikayelovich.task.util.exceptions.ServerSideException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.io.FileNotFoundException;

@ControllerAdvice
public class ExceptionHandlerController {

    /**
     * Handles exceptions which were caused by incorrect data sent by the client.
     * @param exception Exception
     * @return Response entity with error message and error code
     */
    @ExceptionHandler({IllegalArgumentException.class, FileNotFoundException.class})
    public ResponseEntity<String> handleClientSideExceptions(Exception exception) {
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
    }

    /**
     * Handles exceptions which were caused by server side errors.
     * @param exception Exception
     * @return Response entity with error message and error code
     */
    @ExceptionHandler({ServerSideException.class})
    public ResponseEntity<String> handleServerSideExceptions(Exception exception) {
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }


}
