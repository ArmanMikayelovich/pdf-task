package am.mikayelovich.task.conroller;

import am.mikayelovich.task.service.PdfService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping
public class PdfController {

    private final PdfService pdfService;

    public PdfController(PdfService pdfService) {
        this.pdfService = pdfService;
    }

    @PostMapping(value = "/files", consumes = "application/json")
    public String mergeAndStampPdfFiles(@RequestBody List<String> files) throws IOException {
        return pdfService.checkAndProcessFiles(files);
    }

}
