package am.mikayelovich.task.util.exceptions;

/**
 * Wrapper class for RuntimeException
 */
public class ServerSideException extends RuntimeException {
    public ServerSideException(String message) {
        super(message);
    }
}
