package am.mikayelovich.task.service.impl;

import am.mikayelovich.task.util.exceptions.ServerSideException;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class PdfServiceTest {
    PdfServiceImpl pdfService = new PdfServiceImpl(null);

    String resourcePath;
    List<String> sourceList;

    @BeforeEach
    void init() {

        Path resourceDirectory = Paths.get("src", "test", "resources");
        resourcePath = resourceDirectory.toFile().getAbsolutePath();

        sourceList = new ArrayList<>(
                Arrays.asList(resourcePath + "\\1.pdf",
                        resourcePath + "\\2.pdf",
                        resourcePath + "\\3.pdf",
                        resourcePath + "\\5.pdf",
                        resourcePath + "\\6.pdf",
                        resourcePath + "\\7.pdf"
                ));
    }

    @Test
    void test_pagesCountOfDestinationPDFDocument() {
        int pagesCount = 0;
        final String filename = "output_test.pdf";
        try (final PdfWriter pdfWriter = new PdfWriter(filename);
             final PdfDocument writingDocument = new PdfDocument(pdfWriter)) {


            pdfService.copyPagesToDestinationPdf(sourceList, writingDocument);

            for (String origin : sourceList) {
                try (final PdfReader pdfReader = new PdfReader(origin);
                     final PdfDocument readingDocument = new PdfDocument(pdfReader)) {

                    pagesCount += readingDocument.getNumberOfPages();
                } catch (IOException exception) {
                    throw new ServerSideException(exception.getMessage());
                }

            }

            Assertions.assertEquals(pagesCount, writingDocument.getNumberOfPages());

        } catch (IOException exception) {
            throw new ServerSideException(exception.getMessage());
        }

        Assertions.assertTrue(new File(filename).delete());

    }
}
