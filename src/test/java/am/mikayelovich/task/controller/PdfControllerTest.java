package am.mikayelovich.task.controller;

import am.mikayelovich.task.util.exceptions.ServerSideException;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@AutoConfigureMockMvc
class PdfControllerTest {
    List<String> sourceList;
    HttpHeaders headers;
    String resourcePath;
    private static final String OUTPUT = "output.pdf";


    @LocalServerPort
    private int port;

    private String URL;


    @BeforeAll
    public void init() {
        Path resourceDirectory = Paths.get("src", "test", "resources");
        resourcePath = resourceDirectory.toFile().getAbsolutePath();



        URL = "http://localhost:" + port + "/files";
        sourceList = new ArrayList<>(
                Arrays.asList(resourcePath + "\\1.pdf",
                        resourcePath + "\\2.pdf",
                        resourcePath + "\\3.pdf",
                        resourcePath + "\\5.pdf",
                        resourcePath + "\\6.pdf",
                        resourcePath + "\\7.pdf"
                ));

        headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, "application/json");
    }


    TestRestTemplate restTemplate = new TestRestTemplate();


    @Test
    void test_SendEmptyList_400_Error() {
        HttpEntity<List<String>> entity = new HttpEntity<>(new ArrayList<>(), headers);
        ResponseEntity<String> response = restTemplate.exchange(
                URL, HttpMethod.POST, entity, String.class);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    void test_SendListWithNonExistentFiles_400_Error() {
        final List<String> invalidSourceList = sourceList.stream()
                .map(source -> source.substring(1)).collect(Collectors.toList());

        HttpEntity<List<String>> entity = new HttpEntity<>(invalidSourceList, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                URL, HttpMethod.POST, entity, String.class);

        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }


    @Test
    void test_SendListWithNonPdfFiles_400_Error() {
        final ArrayList<String> sourceWithNonPdfFile = new ArrayList<>(sourceList);
        sourceWithNonPdfFile.add(resourcePath + "\\4.png");

        HttpEntity<List<String>> entity = new HttpEntity<>(sourceWithNonPdfFile, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                URL, HttpMethod.POST, entity, String.class);

        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    void test_SendList_ReceiveAbsolutePath_AndCheckPathOfOutput_200_Success() {
        deleteOutputFile();
        HttpEntity<List<String>> entity = new HttpEntity<>(sourceList, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                URL, HttpMethod.POST, entity, String.class);

        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());


        final String userDirectory = System.getProperty("user.dir");
        String checkingPath = userDirectory + "\\" + OUTPUT;
        Assertions.assertEquals(checkingPath, response.getBody());
    }

    private void deleteOutputFile() {
        final File output = new File(OUTPUT);
        if (output.exists()) {
            if (!output.delete()) {
                throw new ServerSideException("Unable to delete file : " + output.getAbsolutePath());
            }
        }
    }
}
